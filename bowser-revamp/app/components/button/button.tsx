import * as React from "react"
import { TouchableOpacity } from "react-native"
import { Text } from "../"
import { theme } from "../../theme"
import { ButtonProps } from "./button.props"
import { mergeAll, flatten } from "ramda"

/**
 * For your text displaying needs.
 *
 * This component is a HOC over the built-in React Native one.
 */
export function Button(props: ButtonProps) {
  // grab the props
  const {
    preset = "primary",
    tx,
    text,
    style: styleOverride,
    textStyle: textStyleOverride,
    children,
    ...rest
  } = props

  const viewStyle = mergeAll(flatten([theme.presets.getByKey(preset, "button", "view") || theme.presets.getByKey("primary", "button", "view"), styleOverride]));
  const textStyle = mergeAll(flatten([theme.presets.getByKey(preset, "button", "text") ||  theme.presets.getByKey("primary", "button", "text"), textStyleOverride]));
  const content = children || <Text tx={tx} text={text} style={textStyle} />

  return (
    <TouchableOpacity style={viewStyle} {...rest}>
      {content}
    </TouchableOpacity>
  )
}
