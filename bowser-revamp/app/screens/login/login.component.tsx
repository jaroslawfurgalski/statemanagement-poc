import React from 'react';
import { View } from 'react-native';
import { Wallpaper, Screen, Header } from "../../components"
import { theme } from "../../theme"
import { LoginStyles as styles, LoginProps } from './';

/*
If your screen/component needs to be state aware, uss a class component instead of a FunctionComponent (FC)
*/
export const Login: React.FC<LoginProps> = (props) => {

    const onSubmit = React.useMemo(() => () => props.navigation.navigate("demo"), [props.navigation]);
    const goBack = React.useMemo(() => () => props.navigation.goBack(null), [props.navigation])

    return (
        <View style={styles.full}>
            <Wallpaper />
            <Screen style={styles.container} preset="scroll" backgroundColor={theme.color.transparent}>
                <Header headerText="Login" leftIcon="back" onLeftPress={goBack} style={theme.style.header} titleStyle={theme.style.headerTitle} />
            </Screen>
        </View>
    );
}