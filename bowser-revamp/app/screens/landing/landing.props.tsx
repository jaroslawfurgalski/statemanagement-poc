import { NavigationScreenProps } from "react-navigation"

export interface LandingProps extends NavigationScreenProps<{}> { }
