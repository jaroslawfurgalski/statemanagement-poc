import React from 'react';
import { View } from 'react-native';
import { Wallpaper, Screen, Header, Text, Button } from "../../components"
import { theme } from "../../theme"
import { LandingStyles as styles, LandingProps } from './';

/*
If your screen/component needs to be state aware, uss a class component instead of a FunctionComponent (FC)
*/
export const Landing: React.FC<LandingProps> = (props) => {


    const onSubmit_ = () => {
        console.log("navigate");
        props.navigation.navigate("demo");

    } 

    const onSubmit = React.useMemo(() => () => props.navigation.navigate("demo"), [
        props.navigation,
    ]);
    
    return (
        <View style={styles.full}>
            <Wallpaper />
            <Screen style={styles.container} preset="scroll" backgroundColor={theme.color.transparent}>
                <Header headerText="CT Technical Operations" leftIcon="back" style={theme.style.header} titleStyle={theme.style.headerTitle} />
            </Screen>
            <Text style={styles.title} preset="header" text="Template Prototype" />
            <Text style={styles.tagLine} text="This is a simple app to demonstrate some concepts and look at how we can structure things to separate concerns, ensure the app is simple to maintain and promote re-use of code etc" />
            <View>
                <Button
                    text="Get started..."
                    onPress={onSubmit}
                />
            </View>
        </View>
    );
}