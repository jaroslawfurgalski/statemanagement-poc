import { StyleSheet } from 'react-native';
import { spacing } from '../../theme/spacing';
  
/*
Styles purely for use by this screen. If styles are globally effectual, use "themes" instead
*/
export const LandingStyles = StyleSheet.create({
    full: {
        flex: 2,
        justifyContent: "flex-start",
    },
    container: {
        padding: 10,
    },
    title: {
        fontSize: 28,
        lineHeight: 38,
        textAlign: "center",
        marginBottom: spacing[5],      
    },
    tagLine: {
        color: "#BAB6C8",
        padding: 10,
        fontSize: 15,
        lineHeight: 22,
        marginBottom: spacing[4] + spacing[1],      
    }
});
