import { color } from "./color";
import { style, presets } from "./style";

export * from "./spacing";
export * from "./typography";
export * from "./timing";
export { ThemePresetNames } from "./style";

export const theme = {
    color: color,
    style: style,
    presets: presets,
};