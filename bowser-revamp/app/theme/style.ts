import { StyleSheet, ViewStyle } from 'react-native';
import { color } from "./color";
import { spacing } from "./spacing";

/* Default styling attributes for various control types */
const _defaults = {
    button: {
        view: {
            paddingVertical: spacing[2],
            paddingHorizontal: spacing[2],
            borderRadius: 4,
            justifyContent: "center",
            alignItems: "center",          
        },
        text: {
            paddingHorizontal: spacing[3],
        }
    },
}

/* Base interface for style preset mapping, we'll extend later so we can keep this just as a clean presentation of supported theme preset names */
interface StylePresetBase {
    primary: any,
    secondary?: any,
}
export type ThemePresetParameter = "text" | "view";

/* And then populate this type mapping from the keys in the above type, will help ensure we don't reference an invalid theme preset type later */
export type ThemePresetNames = keyof StylePresetBase;
/* Same goes for supported controls that we support preset based mapping for */
export type ThemePresetControlNames = keyof typeof _defaults;

interface StylePresets extends StylePresetBase {
    getByKey(presetKey: ThemePresetNames, control: ThemePresetControlNames, presetParameter?: ThemePresetParameter): ViewStyle,
}

export const presets: StylePresets = {
    primary: {
        button: {
            view: {
                ..._defaults.button.view,
                backgroundColor: color.primary

            } as ViewStyle,
            text: {
                ..._defaults.button.text,
                fontSize: 14, 
                color: color.palette.white
            } as ViewStyle
        }
    },
    getByKey(presetKey: ThemePresetNames, control: ThemePresetControlNames, presetParameter?: ThemePresetParameter) {
        if (presetParameter)
            return (this[presetKey] && this[presetKey][control] && this[presetKey][control][presetParameter]) ? this[presetKey][control][presetParameter] : undefined;
        else
            return (this[presetKey] && this[presetKey][control]) ? this[presetKey][control] : undefined;
    },
};

export const style = StyleSheet.create({
    header: {
        paddingTop: spacing[3],
        paddingBottom: spacing[4] + spacing[1],
        paddingHorizontal: 0,
    },
    headerTitle: {
        color: color.palette.white,
        fontFamily: "Montserrat",
        fontWeight: "bold",
        fontSize: 16,
        lineHeight: 18,
        textAlign: "center",
        letterSpacing: 1.5,      
    }
});