import React from "react"
import {AppRegistry} from "react-native"
import {name as appName} from "../app.json"
import { uiTheme } from "./theme"
import { ThemeContext, getTheme } from "react-native-material-ui"
import { Navigator } from "./navigation"

/**
 * PW - LIFTED FROM IGNITE/BOWSER
 * Storybook still wants to use ReactNative's AsyncStorage instead of the
 * react-native-community package; this causes a YellowBox warning. This hack
 * points RN's AsyncStorage at the community one, fixing the warning. Here's the
 * Storybook issue about this: https://github.com/storybookjs/storybook/issues/6078
 */
const ReactNative = require("react-native");
Object.defineProperty(ReactNative, "AsyncStorage", {
  get(): any {
    return require("@react-native-community/async-storage").default
  },
})


export const App: React.FC<{}> = () => {
  return (
    <ThemeContext.Provider value={getTheme(uiTheme)}>
      <Navigator.withHeader />
    </ThemeContext.Provider>
  );
}

// PW - ALSO LIFTED FROM IGNITE/BOWSER
// Should we show storybook instead of our app?
//
// ⚠️ Leave this as `false` when checking into git.
const SHOW_STORYBOOK = false

let RootComponent = App
if (__DEV__) {
  // PW - UNCOMMENT THIS WHEN STORYBOOK HAS BEEN BROUGHT IN PROPERLY
  //const { StorybookUIRoot } = require("../storybook")
  //if (SHOW_STORYBOOK) RootComponent = StorybookUIRoot
}

AppRegistry.registerComponent(appName, () => RootComponent)

