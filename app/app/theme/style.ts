import { ImageStyle, ViewStyle, StyleSheet, TextStyle, Dimensions } from "react-native"
import { color } from "./color"
/*
Segment styling by control then "preset"
*/
export const controlPresets = {
    wallpaper: {
        stretch: {
            position: "absolute",
            top: 0,
            left: 0,
            bottom: 0,
            right: 0,
            resizeMode: "stretch",
            width: null, // Have to set these to null because android ¯\_(ツ)_/¯
            height: null,
        } as ImageStyle
    },
    screen: {
        fixed: {
            outer: {
                backgroundColor: color.background,
                flex: 1,
                height: "100%",
              } as ViewStyle,
              inner: {
                justifyContent: "flex-start",
                alignItems: "stretch",
                height: "100%",
                width: "100%",
              } as ViewStyle,          
        },
        scroll: {
            outer: {
                backgroundColor: color.background,
                flex: 1,
                height: "100%",
            } as ViewStyle,
            inner: { justifyContent: "flex-start", alignItems: "stretch" } as ViewStyle,          
        }
    },
    textblock: {
        header: {
            fontSize: 28,
        },
        title: {
            fontSize: 20,
        },
        content: {
        }
    }
}

export const defaultStyles = StyleSheet.create({
    screen: {
        flex:1,
        alignItems:'center',
        justifyContent:'center'
    } as ViewStyle,
    section: {
        flexDirection:'column',
        alignItems:'center',
        justifyContent:'center',
        padding: 16,
    } as ViewStyle,
    textblock: {
        width: Dimensions.get('window').width-32,
    } as TextStyle,
    textInput: {
        borderBottomColor: color.primary,
        borderBottomWidth: 1,
        marginBottom: 16,
        width: Dimensions.get('window').width-32,
    },
    controlWrapper: {
        padding: 20
    } as ViewStyle
})

/*
Map out control types and presets per control type we support based on what we've defined in controlStyles
This should allow us a degree of type safety in referencing stuff that we know will be valid.
*/
export type ControlTypesWithPresets = keyof typeof controlPresets  // Yields: "wallpaper", "screen"

export type WallpaperPreset = keyof typeof controlPresets.wallpaper  // Yields: "stretch"

export type ScreenPreset = keyof typeof controlPresets.screen  // Yields: "fixed", "scroll"

export type TextblockPreset = keyof typeof controlPresets.textblock // Yields: "header", "title", "content"

export type ColorScheme = "primary" | "primaryAccent" | "secondary" | "secondaryAccent" | "dark"


