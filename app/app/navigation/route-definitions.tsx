import { LoginScreen, HomeScreen, MilageScreen } from "../screens"

/*
Available app routes are defined here, to keep route definition
clean and abstrated away from the navigator implementation itself.
This way, things like upgrading to RN nav 5 later when it's available
will be fairly trivial.
*/

/*
Note about headers, as seen in the Home route below, you can set header: null to override
the navigator behaviours that I've implemented, so even if you introduce Navigator.withHeader,
any route specified with a null header will not display a header regardless.
TODO Add in the style and titleStyle attributes and create default theming for them
*/
export const appRoutes = {
    Login: { 
        screen: LoginScreen,
        navigationOptions: {
            title: "Login"
        }
    },
    Home: { 
        screen: HomeScreen,
        navigationOptions: {
            title: "Welcome!",
            header: null
        }
    },
    Milage: { 
        screen: MilageScreen,
        navigationOptions: {
            title: "Milage",
            header: null
        }
    }
}