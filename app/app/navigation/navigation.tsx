import { createAppContainer } from "react-navigation"
import { createStackNavigator } from "react-navigation-stack"
import { appRoutes } from "./route-definitions"

function appContainer(withHeader: boolean): any {
    const _appNavigator = createStackNavigator(appRoutes, {
        initialRouteName: "Milage",
        headerMode: withHeader ? "float" : "none"
    })
    return createAppContainer(_appNavigator)
}

export type NavigatorType = "default"

export const Navigator:any = {
    default: appContainer(false),
    withHeader: appContainer(true),
}