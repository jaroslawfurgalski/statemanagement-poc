import { ViewStyle } from "react-native"
import { TextblockPreset, ColorScheme } from "../../theme"

export interface TextblockProps {
  style?: ViewStyle,
  preset?: TextblockPreset,
  scheme?: ColorScheme,
}
