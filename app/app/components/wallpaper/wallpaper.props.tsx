import { ImageStyle } from "react-native"
import { WallpaperPreset } from "../../theme"

export interface WallpaperProps {
  style?: ImageStyle
  backgroundImage?: string
  preset?: WallpaperPreset
}
