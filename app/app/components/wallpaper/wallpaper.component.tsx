import React from "react"
import { ImageBackground } from "react-native"
import { theme, WallpaperPreset, ControlTypesWithPresets } from "../../theme"
import { WallpaperProps } from "./wallpaper.props"

/*
Full page wallpaper
*/
export const Wallpaper: React.FC<WallpaperProps> = (props) => {
    const presetControlType: ControlTypesWithPresets = "wallpaper"
    const defaultPreset: WallpaperPreset = "stretch"
    const style = props.style || { ...theme.controlPresets[presetControlType][props.preset||defaultPreset] }
    const source = props.backgroundImage || require("../../assets/bg.jpg")
    return (
        <ImageBackground source={source} style={style}>
            {props.children}
        </ImageBackground>
    )
}
