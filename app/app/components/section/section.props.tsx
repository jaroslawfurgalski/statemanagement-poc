import { ViewStyle } from "react-native"

export interface SectionProps {
  style?: ViewStyle,
}
