import { ViewStyle } from "react-native"

export interface ScreenProps {
  style?: ViewStyle
}
