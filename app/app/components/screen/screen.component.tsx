import React from "react"
import { View } from "react-native"
import { theme} from "../../theme"
import { ScreenProps } from "./screen.props"

/*
A simple wrapper component to host the content for a page/screen
*/
export const Screen: React.FC<ScreenProps> = (props) => {
    const style = props.style || { ...theme.defaults.screen }
    return (
        <View style={style}>
            {props.children}
        </View>
    )
}
