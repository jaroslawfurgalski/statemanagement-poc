import { 
    AuthenticationService,
    AuthenticationRequest,
    AuthenticatedUser
} from "../types"

export class AppAuthenticationService implements AuthenticationService {
    async login(authenticationRequest: AuthenticationRequest): Promise<AuthenticatedUser> {
        return await undefined
    }    
    async logout(): Promise<boolean> {
        return await false
    }
    async isLoggedIn(): Promise<boolean> {
        return await false
    }
}