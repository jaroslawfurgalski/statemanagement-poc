/*
Our services should implement interfaces as it will make it viable for us
to test via the abstration against mocked versions etc.
*/
export interface AuthenticationRequest {
    username: string
    password: string
}
export interface AuthenticatedUser {
    id: string
    username: string
    displayName: string
}
type AsyncLoginFunction = (authenticationRequest: AuthenticationRequest) => Promise<AuthenticatedUser>
type AsyncBooleanFunction = () => Promise<boolean>

export interface AuthenticationService {
    login: AsyncLoginFunction
    logout: AsyncBooleanFunction
    isLoggedIn: AsyncBooleanFunction
}