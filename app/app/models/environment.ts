import { AuthenticationService } from "../services/types"
import { AppAuthenticationService } from "../services/authentication/authentication-service"

let ReactotronDev
if (__DEV__) {
  const { Reactotron } = require("../services/reactotron")
  ReactotronDev = Reactotron
}

/**
 * The environment is a place where services and shared dependencies between
 * models live.  They are made available to every model via dependency injection.
 */
export class Environment {
  constructor() {
    // create each service
    if (__DEV__) {
      // dev-only services
      this.reactotron = new ReactotronDev()
    }
    this.authenticationService = new AppAuthenticationService()
    /*
    this.api = new Api()
    this.messaging = new Messaging()
    */
  }

  async setup() {
    if (__DEV__) {
      await this.reactotron.setup()
    }
    //await this.api.setup()
    //await this.messaging.setup()
  }

  reactotron: typeof ReactotronDev

  authenticationService: AuthenticationService

  //api: Api

  //messaging: Messaging
}