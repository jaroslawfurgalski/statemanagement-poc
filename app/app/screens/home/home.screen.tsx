import React from "react"
import { Image } from "react-native"
import { Button } from "react-native-material-ui"
import { HomeProps } from "./home.props"
import { Wallpaper, Screen, Section, Textblock } from "../../components"

/*
If your screen/component needs to be state aware, uss a class component instead of a FunctionComponent (FC)
*/
export const HomeScreen: React.FC<HomeProps> = (props) => {
    const submit = () => { 
        props.navigation.navigate("Login")
    }

    const logo =  require("../../assets/logo.png")
    return (
        <>
        <Wallpaper />
        <Screen>
            <Section>
                <Image source={logo}></Image>
                <Textblock preset="header">TO Application Template</Textblock>
            </Section>
            <Section>
                <Textblock scheme="dark">A simple templated app with some usage examples to help get you started. We've included a few custom elements, examples of clean layout and separation of concerns, preferred packages and standards and abstration for things like authentication.</Textblock>
            </Section>
            <Section>
                <Button text="Continue" raised primary onPress={submit} icon="play-arrow">
                </Button>
            </Section>
        </Screen>
    </>
    )
}