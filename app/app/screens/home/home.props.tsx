import { NavigationInjectedProps } from "react-navigation"

export interface HomeProps extends NavigationInjectedProps<{}> { }