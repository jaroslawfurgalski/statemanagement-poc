import * as React from 'react';
import { View, KeyboardAvoidingView, TextInput } from "react-native"
import { Button } from "react-native-material-ui"
import { LoginProps } from "./login.props"
import { theme } from "../../theme"
import { Screen, Section, Textblock } from "../../components"


/*
If your screen/component needs to be state aware, uss a class component instead of a FunctionComponent (FC)
*/
export const LoginScreen: React.FC<LoginProps> = (props) => {
    const submit = () => { 
        props.navigation.goBack();
    }

    return (
        <Screen>
            <KeyboardAvoidingView>
                <Section>
                    <Textblock>Username</Textblock>
                    <TextInput style={theme.defaults.textInput}></TextInput>
                    <Textblock>Password</Textblock>
                    <TextInput style={theme.defaults.textInput}></TextInput>
                </Section>
                <Section>
                    <Button text="Login" raised primary onPress={submit} icon="done">
                    </Button>
                </Section>
            </KeyboardAvoidingView>
        </Screen>
    )
}