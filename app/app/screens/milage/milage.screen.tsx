import * as React from 'react';
import { View, KeyboardAvoidingView, TextInput } from "react-native"
import { Button } from "react-native-material-ui"
import { MilageProps } from "./milage.props"
import { theme } from "../../theme"
import { Screen, Section, Textblock } from "../../components"


/*
If your screen/component needs to be state aware, uss a class component instead of a FunctionComponent (FC)
*/
export const MilageScreen: React.FC<MilageProps> = (props) => {
    const submit = () => { 
        props.navigation.goBack();
    }

    return (
        <Screen>
            <KeyboardAvoidingView>
                <Section>
                    <Textblock>Test</Textblock>
                    <TextInput style={theme.defaults.textInput}></TextInput>
                    <Textblock>Test</Textblock>
                    <TextInput style={theme.defaults.textInput}></TextInput>
                </Section>
                <Section>
                    <Button text="Login" raised primary onPress={submit} icon="done">
                    </Button>
                </Section>
            </KeyboardAvoidingView>
        </Screen>
    )
}